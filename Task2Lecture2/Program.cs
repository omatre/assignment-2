﻿using System;

namespace Task2Lecture2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Do you want to print a square or a rectangle? (S/R)");
            char choice = Console.ReadKey().KeyChar;
            Console.WriteLine();

            if (choice == 'S' || choice == 's')
            {
                Console.WriteLine("Input size of the side of a square as an integer...");
                int.TryParse(Console.ReadLine(), out int sideOneSize);

                PrintSquare(sideOneSize);
            } 
            else if (choice == 'R' || choice == 'r')
            {
                Console.WriteLine("Input size of both sides of a rectangle as integers...");
                int.TryParse(Console.ReadLine(), out int sideOneSize);
                int.TryParse(Console.ReadLine(), out int sideTwoSize);

                PrintRectangle(sideOneSize, sideTwoSize);
            }
            else
            {
                Console.WriteLine("No valid choice made!");
            }
        }

        private static void PrintRectangle(int sideOneSize, int sideTwoSize)
        {
            for (int i = 0; i < sideOneSize; i++)
            {
                for (int j = 0; j < sideTwoSize; j++)
                {
                    if (i == 0 || i == sideOneSize - 1 || j == 0 || j == sideTwoSize - 1)
                    {
                        Console.Write("# ");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine();
            }
        }

        private static void PrintSquare(int sideOneSize)
        {
            for (int i = 0; i < sideOneSize; i++)
            {
                for (int j = 0; j < sideOneSize; j++)
                {
                    if (i == 0 || i == (sideOneSize - 1) || j == 0 || j == (sideOneSize - 1))
                    {
                        Console.Write("# ");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
